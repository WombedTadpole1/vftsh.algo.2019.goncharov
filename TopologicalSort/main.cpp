#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::vector;

void dfs(int vertex, vector<vector<int>> &graph, vector<bool> &visited, vector<int> &sorted) {
    visited[vertex] = 1;
    for (int i = 0; i < graph[vertex].size(); ++i) {
        if(visited[graph[vertex][i]] == 0) {
            dfs(graph[vertex][i], graph, visited, sorted);
        }
    }
    sorted.push_back(vertex);
}

void TopSort(vector<vector<int>> &graph, vector<bool> &visited, vector<int> &sorted) {
        for(int i = 0; i < visited.size(); ++i) {
            if (visited[i] == 0) {
                dfs(i, graph, visited, sorted);
            }
        }
}

int main() {
    vector<int> topSortedDots;
    vector<vector<int>> graph(5);
    vector<bool> visited(5);
    graph[0].push_back(1);
    graph[0].push_back(2);
    graph[0].push_back(3);
    graph[0].push_back(4);
    graph[1].push_back(3);
    graph[1].push_back(4);
    graph[2].push_back(4);
    graph[4].push_back(3);
/*    graph[3].push_back(NULL);
    graph[4].push_back(NULL);*/
    /*наш граф(ориентированный, ацикличный):
     0[1, 2, 3, 4]
     1[3, 4]
     2[4]
     3[]
     4[3]
     */
    TopSort(graph, visited, topSortedDots);
    for (int i = topSortedDots.size() - 1; i >= 0; --i) {
        cout << topSortedDots[i] << " ";
    }
    return 0;
}

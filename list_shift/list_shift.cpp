#include <iostream>
#include <list>

using std::cin;
using std::cout;
using std::list;

int main() {
    list<int> myList;
    int n, x, k;
    cout << "Enter the number of elements in your list" << "\n";
    cin >> n;
    cout << "Enter elements" << "\n";
    for (int i = 0; i < n; ++i) {
        cin >> x;
        myList.push_back(x);
    }
    cout << "Enter how many elements you want to shift" << "\n";
    cin >> k;
    auto it = myList.begin();
    advance(it, k);
    myList.insert(myList.end(), myList.begin(), it);
    myList.erase(myList.begin(), it);
    for (auto &item : myList) {
        cout << item <<  " ";
    }
    return 0;
}

//
// Created by Женёк on 28.12.2019.
//

#ifndef PLEASEHEAP_HEAP_H
#define PLEASEHEAP_HEAP_H
#include <iostream>
#include <vector>

using std::vector;

class THeap {
private:
    std::vector<int> Heap;
    void Swap(int indexA, int indexB) {
        int help = Heap[indexA];
        Heap[indexA] = Heap[indexB];
        Heap[indexB] = help;
    }

    void SiftUp (int index) {
        if (Heap.size() == 1) {
        } else {
            if (index % 2 == 0 && Heap[index] > Heap[(index - 2) / 2]) {
                  Swap(index, (index - 2) / 2);
                if ((index - 2) / 2 > 0)
                    SiftUp((index - 2) / 2);
            } else if (index % 2 == 1 && Heap[index] > Heap[(index - 1) / 2]) {
                  Swap(index, (index - 1) / 2);
                if ((index - 1) / 2 > 0)
                    SiftUp((index - 1) / 2);
            }
        }
    }

    void SiftDown(int index) {
        bool pullDown = true;
        int leftChild = index * 2 + 1;
        int rightChild = index * 2 + 2;
        int theBiggest;
        if (rightChild < Heap.size()) {
            if (Heap[leftChild] > Heap[rightChild]) {
                theBiggest = leftChild;
            }
            else {
                theBiggest = rightChild;
            }
            if (Heap[index] < Heap[theBiggest]) {
                Swap(theBiggest, index);
            }
            else {
                pullDown = false;
            }
        }
        else if (leftChild == Heap.size() - 1) {
            theBiggest = leftChild;
            if (Heap[index] < Heap[theBiggest]) {
                Swap(theBiggest, index);
            }
                pullDown = false;
        }
        else {
            pullDown = false;
        }
        if (pullDown) {
            SiftDown(theBiggest);
        }
    }

public:
    THeap() {}

    void AddEl(int x) {
        Heap.push_back(x);
        SiftUp(Heap.size() - 1);
    }

    int RemoveTop() {
        int x = Heap[0];
        Heap[0] = Heap.back();
        Heap.pop_back();
        SiftDown(0);
        return x;
    }

    bool Empty() {
        return  Heap.size() == 0;
    }

    int Size() {
        return Heap.size();
    }
};


#endif //PLEASEHEAP_HEAP_H

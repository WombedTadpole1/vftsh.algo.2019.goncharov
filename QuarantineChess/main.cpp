#include <iostream>

using std::cin;
using std::cout;


void gen(int a, int b,int f[8][8]) {
    for (int i = 1; i < a; ++i){
        for (int j = 1; j < b; ++j) {
            if (f[i][j] != 0){
                f[i][j] = f[i - 1][j] + f[i - 1][j - 1] + f[i][j - 1];
            }
        }
    }
}


int main()
{
    int n, x, y;

    int field[8][8];

    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            field[i][j] = -1;
        }
    }
    for (int i = 0; i < 8; ++i) {
        field[i][0] = 1;
    }
    for (int j = 0; j < 8; ++j) {
        field[0][j] = 1;
    }

    cin >> n;
    for (int i = 1; i <= n; ++i) {
        cin >> x >> y;
        field[x - 1][y - 1] = 0;
    }

    gen( 8, 8, field);

    cout << field[7][7];

    return 0;
}
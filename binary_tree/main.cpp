#include <iostream>
#include "tree.h"

using std::cin;
using std::cout;

int main()
{
    TTree newtree;
    int List[8] = { 15, 12, 20, 22, 7, 4, 2, 8};
    for (int i = 0; i < 8; ++i) {
        newtree.AddNode(List[i]);
    }
    

    newtree.printList();
    newtree.DeleteNode(4);
    newtree.printList();

    return 0;
}

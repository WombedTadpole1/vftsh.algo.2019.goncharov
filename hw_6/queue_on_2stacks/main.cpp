#include <iostream>
#include "t_queue.h"

using std::cin;
using std::cout;

int main() {
    TQueue queue;
    queue.Push(5);
    queue.Push(25);
    queue.Push(30);
    queue.Push(41);
    cout << "size = " << queue.Size() << "\n";
    cout << "top = " << queue.Pop() << "\n";
    cout << "top = " << queue.Pop() << "\n";
    cout << "epmty? " << queue.Empty() << "\n";
    return 0;
}

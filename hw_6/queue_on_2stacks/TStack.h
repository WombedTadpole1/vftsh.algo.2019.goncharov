//
// Created by Женёк on 18.12.2019.
//

#ifndef STACKQUEUE_TSTACK_H
#define STACKQUEUE_TSTACK_H


struct TStackEl {
    int Data;
    TStackEl* Next;
};

class TStack {
    unsigned int Len;
    TStackEl* Head;
public:

    TStack() : Len(0), Head(nullptr) {}

    void Pop() {
            TStackEl* deletedEl = Head;
            Head = Head->Next;
            delete deletedEl;
            --Len;
    }

    int Top() {
            return Head->Data;
    }

    void Push(int x) {
        TStackEl* newEl = new TStackEl;
        newEl->Data = x;
        newEl->Next = Head;
        Head = newEl;
        ++Len;
    }

    bool Empty() {
        return Len == 0;
    }

    unsigned int Size() {
        return Len;
    }
};


#endif //STACKQUEUE_TSTACK_H

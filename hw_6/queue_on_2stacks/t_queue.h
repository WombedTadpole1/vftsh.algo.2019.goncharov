//
// Created by Женёк on 18.12.2019.
//

#ifndef STACKQUEUE_T_QUEUE_H
#define STACKQUEUE_T_QUEUE_H

#include <iostream>
#include "TStack.h"

class TQueue {
private:
    TStack Main;
    TStack Auxiliary;
public:

    TQueue() {}

    void Push(int x) {
        Main.Push(x);
    }

    int Pop() {
            int size = Main.Size();
            int top;
            if (Auxiliary.Empty()) {
                for (int i = 1; i < size; ++i) {
                    Auxiliary.Push(Main.Top());
                    Main.Pop();
                }
                top = Main.Top();
                Main.Pop();
            }
            else {
                top = Auxiliary.Top();
                Auxiliary.Pop();
            }
            return top;
        }

    unsigned int Size() {
        return Main.Size() + Auxiliary.Size();
    }

    bool Empty() {
        if (Main.Empty() && Auxiliary.Empty()) {
            return 1;
        }
        else {
            return 0;
        }
    }
};


#endif //STACKQUEUE_T_QUEUE_H

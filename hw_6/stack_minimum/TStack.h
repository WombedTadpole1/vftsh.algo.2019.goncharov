//
// Created by Женёк on 19.12.2019.
//

#ifndef STACKMINIMUM_TSTACK_H
#define STACKMINIMUM_TSTACK_H


struct TStackEl {
    int Min;
    int Data;
    TStackEl* Next;
};

class TStack {
    unsigned int Len;
    TStackEl* Head;
public:

    TStack() : Len(0), Head(nullptr) {}

    void Pop() {
        TStackEl* deletedEl = Head;
        Head = Head->Next;
        delete deletedEl;
        --Len;
    }

    int Top() {
            return Head->Data;
    }

    void Push(int x) {
        TStackEl* newEl = new TStackEl;
        newEl->Data = x;
        if (Empty()) {
            newEl->Min = x;
        }
        else {
            if (Head->Min > x) {
                newEl->Min = x;
            }
            else {
                newEl->Min = Head->Min;
            }
        }
        newEl->Next = Head;
        Head = newEl;
        ++Len;
    }

    bool Empty() {
        return Len == 0;
    }

    unsigned int Size() {
        return Len;
    }

    int Min() {
        return Head->Min;
    }
};



#endif //STACKMINIMUM_TSTACK_H

#include <iostream>
#include "TStack.h"

using std::cout;

int main() {
    TStack stack;
    stack.Push(5);
    stack.Push(25);
    cout << stack.Min() << "\n";
    stack.Pop();
    stack.Push(3);
    cout << stack.Min() << "\n";
    return 0;
}
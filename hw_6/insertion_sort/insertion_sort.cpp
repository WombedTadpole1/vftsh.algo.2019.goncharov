#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::vector;

void Swap(vector<int>& array,int indexA, int indexB) {
    int help = array[indexA];
    array[indexA] = array[indexB];
    array[indexB] = help;
}

void InsertionSort(vector<int>& a) {
    if(a.size() > 1) {
        int i = 0;
        int j = 1;
        int nonSortedIndex = j;
        while (j <= a.size() - 1) {
            nonSortedIndex = j;
            if (a[j] < a[i]) {
                while (i >= 0 && a[j] < a[i]) {
                    Swap(a, j, i);
                    --j;
                    --i;
                }
            }
            j = nonSortedIndex + 1;
            i = nonSortedIndex;
        }
    }
}

int main() {
    vector<int> array;
    int n;
    cout << "Enter the number of elements" << "\n";
    cin >> n;
    for (int i = 0; i < n; ++i) {
        array.push_back(rand() % 10);
    }
    cout << "Your array:" << "\n";
    for (int i = 0; i < n; ++i) {
        cout << array[i]  << " ";
    }
    cout << "\n";
    InsertionSort(array);
    cout << "Sorted array:" << "\n";
    for (int i = 0; i < n; ++i) {
        cout << array[i] << " ";
    }
    return 0;
}

#include <iostream>
#include "graph.h"
using std::cin;
using std::cout;

int main() {
    int n, m;
    cout << "Enter the number of vertexes" << "\n";
    cin >> n;
    vector<vector<int>> array(n, vector<int>());
        for (int i = 0; i < n; ++i) {
        cout << "Enter the number of connections for " << i <<  " vertex" << "\n";
        cin >> m;
        cout << "Connected vertexes";
        for (int j = 0; j < m; ++j) {
            int x;
            cin >> x;
            array[i].push_back(x);
        }
    }
    TGraph graph(array);
    cout << "Is graph connected? :" << graph.IsGraphConnected() << "\n";
    cout << "Number of connected components: " << graph.TheNumberOfConnectedComponents() << "\n";
    cout << "Is tree? " << graph.IsTree() << "\n";
    cout << "Is circle? "<< graph.IsCircle() << "\n";
    return 0;
}


#ifndef GRAPH_GRAPH_H
#define GRAPH_GRAPH_H

#include <iostream>
#include <queue>
#include <vector>
#include <stack>

using std::stack;
using std::vector;
using std::queue;


class TGraph {
private:
    vector<vector<int>> List;
    vector<int> visitedVertex;
    void Bfs(int vertex) {
        queue<int> queue1;
        queue1.push(vertex);
        while (!queue1.empty()) {
            int elemRow = queue1.front();
            queue1.pop();
            visitedVertex[elemRow] = 1;
            for (int j = 0; j < List[elemRow].size(); ++j) {
                if(visitedVertex[  List[elemRow][j] ] != 1) {
                    queue1.push(List[elemRow][j]);
                }
            }
        }
    }

    void Erase(vector<int>& array) {
        for (int i = 0; i < array.size(); ++i) {
            array[i] = 0;
        }
    }

    bool Circle(unsigned int v) {
        visitedVertex[v] = true;
        int cnt_visited = 0;
        for (unsigned int i = 0; i < List[v].size() && cnt_visited < 2; ++i) {
            if (v == List[v][i]) {
                return true;
            }
            if (visitedVertex[List[v][i]]) {
                ++cnt_visited;
            }
        }
        if (cnt_visited == 2) {
            return true;
        } else {
            bool toReturn = false;
            for (unsigned int i = 0; i < List[v].size() && !toReturn; ++i) {
                if (!visitedVertex[List[v][i]] && Circle(List[v][i])) {

                    toReturn = true;
                }
            }
            return toReturn;
        }
    }

public:
    TGraph(vector<vector<int>>& a) {
        List = a;
        for (int i = 0; i < List.size(); ++i) {
            visitedVertex.push_back(0);
        }
    }

    bool IsGraphConnected() {
        Erase(visitedVertex);
        if (!List.empty()) {
            bool connected = true;
            Bfs(0);
            for (int i = 0; i < visitedVertex.size(); ++i) {
                if (visitedVertex[i] == 0) {
                    connected = false;
                    return connected;
                }
            }
            return connected;
        }
        else {
            return false;
        }
    }

    int TheNumberOfConnectedComponents() {
        Erase(visitedVertex);
        int number = 0;
        if (!List.empty()) {
            for (int i = 0; i < visitedVertex.size(); ++i) {
                if (visitedVertex[i] == 0) {
                    //cout << " i = " << i <<" \n";
                    Bfs(i);
                    ++number;
                }
            }
        }
        else {
            return 0;
        }
        Erase(visitedVertex);
        return number;
    }


    bool IsTree() {
        if (IsGraphConnected() && !IsCircle()) {
            return true;
        }
        else {
            return false;
        }
    }

    bool IsCircle() {
        if (List.size() == 0) {
            return false;
        } else {
            Erase(visitedVertex);
            bool toReturn = Circle(0);
            Erase(visitedVertex);
            return toReturn;
        }
    }

};


#endif //GRAPH_GRAPH_H

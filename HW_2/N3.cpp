#include <iostream>
#include <stack>
#include <string>

int main() {
	std::stack<char> brackets;
	std::string str;
	std::cin >> str;
	if (str.length() % 2 != 0 || str[0] == ']' || str[0] == ')' || str[0] == '}') {
		std::cout << "Incorrect";
		return 0;
	} 
	else {
		for (int i = 0; i < str.length(); i++) {
			if (str[i] == '(' || str[i] == '{' || str[i] == '[') {
				brackets.push(str[i]);
			}
			else {
				if (str[i] == ')') {
					if (!brackets.empty()) {
						if (brackets.top() == '(') {
							brackets.pop();
						}
						else {
							std::cout << "Incorrect";
							return 0;
						}
					}
					else {
						std::cout << "Incorrect";
						return 0;
					}
				}
				if (str[i] == '}') {
					if (!brackets.empty()) {
						if (brackets.top() == '{') {
							brackets.pop();
						}
						else {
							std::cout << "Incorrect";
							return 0;
						}
					}
					else {
						std::cout << "Incorrect";
						return 0;
					}
					
				}	
				if (str[i] == ']') {
					if (!brackets.empty()) {
						if (brackets.top() == '[') {
							brackets.pop();
						}
						else {
							std::cout << "Incorrect";
							return 0;
						}
					}
					else {
						std::cout << "Incorrect";
						return 0;
					}
					
				}
				
			}

		}
	}
	if (brackets.empty()) {
		std::cout << "Correct";
	}
	else {
		std::cout << "Incorrect";
	}
	return 0;
}

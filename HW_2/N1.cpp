#include <iostream>

struct TStackEl {
	int Data;
	TStackEl* Next;
};

class TStack {
	unsigned int Len;
	TStackEl* Head;
public:

	TStack() : Len(0), Head(NULL) {}

	void Pop() {
		if (Empty()) {
			std::cout << "empty";
		}
		else {
			TStackEl* newEl = Head;
			Head = Head->Next;
			delete newEl;
			Len--;
		}
	}

	int Top() {
		if (Empty()) {
			std::cout << "empty" << "\n";
		}
		else {
			return Head->Data;
		}
	}

	void Push(int x) {
		TStackEl* newEl = new TStackEl;
		newEl->Data = x;
		newEl->Next = Head;
		Head = newEl;
		Len++;
	}

	bool Empty() {
		return Len == 0;
	}

	unsigned int Size() {
		return Len;
	}
};
int main() {
	TStack n;
	n.Push(4);
	n.Push(56);
	std::cout << n.Top() << "\n";
	std::cout << "Size = " << n.Size() << "\n";
	std::cout <<  "Is epmty? " << n.Empty() << "\n";

	return 0;
}


#include <iostream>
#include <deque>
int main() {
	int n;
	std::cin >> n;
	int* Numbers = new int[n];
	for (int i = 0; i < n; i++) {
		std::cin >> Numbers[i];
	}
	std::deque<int> NewNumbers;
	for (int i = 0; i < n; i++) {
		NewNumbers.push_front(Numbers[i]);
	}
	while (NewNumbers.size() != 2) {
		if ((NewNumbers.at(0) + NewNumbers.at(1)) % 2 == 0 ) {
			NewNumbers.push_front(NewNumbers.at(0) + NewNumbers.at(1));
			NewNumbers.pop_back();
			NewNumbers.pop_back();
		}
		else {
			NewNumbers.push_front(NewNumbers.at(0) - NewNumbers.at(1));
			NewNumbers.pop_back();
			NewNumbers.pop_back();
		}
	}
	if ((NewNumbers.at(0) + NewNumbers.at(1)) % 2 == 0) {
		std::cout << NewNumbers.at(0) + NewNumbers.at(1) << "\n";
	}
	else {
		std::cout << NewNumbers.at(0) - NewNumbers.at(1) << "\n";
	}

	return 0;
}

#include <iostream>

struct TDeckEl {
    int Data;
    TDeckEl* Next;
    TDeckEl* Previous;
};

class TDeck {
    unsigned long Len;
    TDeckEl* Head;
    TDeckEl* Tail;
public:
    TDeck() : Len(0), Head(nullptr), Tail(nullptr) {}
    void PushBack(int x) {
        if (Len == 0) {
            auto* newEl = new TDeckEl;
            newEl->Data = x;
            newEl->Previous = Tail;
            newEl->Next = Head;
            Tail = newEl;
            Head = newEl;
            ++Len;
        }
        else {
            auto* newEl = new TDeckEl;
            newEl->Data = x;
            newEl->Previous = Tail;
            Tail = newEl;
            ++Len;
        }
    }
    void PushFront(int x) {
        if (Empty()) {
            auto* newEl = new TDeckEl;
            newEl->Data = x;
            newEl->Next = Head;
            newEl->Previous = Tail;
            Tail = newEl;
            Head = newEl;
            ++Len;
        }
        else {
            auto* newEl = new TDeckEl;
            newEl->Data = x;
            newEl->Next = Head;
            Head = newEl;
            ++Len;
        }
    }
    void PopFront() {
        if (Empty()) {
            std::cout << "deque is empty" << "\n";
        }
        else {
            auto * newEl = Head;
            Head = Head->Next;
            delete newEl;
            --Len;
        }
    }
    void PopBack() {
        if (Empty()) {
            std::cout << "deque is empty" << "\n";
        }
        else {
            auto* newEl = Tail;
            Tail = Tail->Previous;
            delete newEl;
            --Len;
        }
    }
    int ShowTail() {
        if (Empty()) {
            return 0;
        }
        else {
            return Tail->Data;
        }
    }
    int ShowTop() {
        if (Empty()) {
            return 0;
        }
        else {
            return Head->Data;
        }
    }
    bool Empty() {
        return Len == 0;
    }
    unsigned long Size() {
        return Len;
    }
};

int main() {
    TDeck n;
    n.PushBack(1);
    n.PushFront(2);
    n.PopBack();

    std::cout<<n.ShowTail();

/*    n.PushFront(23);
    n.PushBack(11);
    std::cout << "Top = " << n.ShowTop() << "\n";
    std::cout << "Tail = " << n.ShowTail() << "\n";
    std::cout << "Size = " << n.Size() << "\n";
    n.PopBack();
    std::cout << "Tail = " << n.ShowTail() << "\n";
    std::cout << "Is Empty? " << n.Empty() << "\n";*/
    return 0;
}
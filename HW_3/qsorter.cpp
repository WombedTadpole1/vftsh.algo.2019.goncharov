#include <iostream>
void Qsort(int* A, int l, int r) {
    int s = (r + l ) / 2;
    int support = A[s];
    int i = l;
    int j = r;
    while (i <= j) {
        while (A[i] < support) {
            ++i;
        }
        while (A[j] > support) {
            --j;
        }
        if (i <= j) {
            int help = A[i];
            A[i] = A[j];
            A[j] = help;
            ++i;
            --j;
        }
    }
    if (l < j) {
        Qsort(A, l, j);
    }
    if (i < r) {
        Qsort(A, i, r);
    }
}
int main() {
    int a;
    std::cout << "Please, enter the size of your array: ";
    std::cin >> a;
    int *A = new int[a];
    std::cout << "Your array: ";
    for (int i = 0; i < a; ++i) {
        A[i] = rand() % 10;
        std::cout << A[i] << " ";
    }
    std::cout << "\n";
    Qsort(A, 0, a - 1);
    std::cout << "Sorted array: ";
    for (int i = 0; i < a; ++i) {
        std::cout << A[i] << " ";
    }
    return 0;
}
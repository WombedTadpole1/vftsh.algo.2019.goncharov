#include <iostream>
void Qsort(int* A, int l, int r) {
    int s = (r + l ) / 2;
    int support = A[s];
    std::cout << "support: " << support << "\n";
    std::cout << "l: " << l << "\n";
    std::cout << "r: " << r << "\n";
    int i = l;
    int j = r;
    while (i <= j) {
        while (A[i] < support) {
            i++;
        }
        while (A[j] > support) {
            j--;
        }
        if (i <= j) {
            int help = A[i];
            std::cout << "A[" << i << "] = " << A[i] << "\n";
            std::cout << "A[" << j << "] = " << A[j] << "\n";
            A[i] = A[j];
            A[j] = help;
            std::cout << "A[" << i << "] = " << A[i] << "\n";
            std::cout << "A[" << j << "] = " << A[j] << "\n";
            for (int k = 0; k < (r - l + 1); ++k) {
                std::cout << A[k] << " ";
            }
            std::cout << "\n ";
            i++;
            j--;
        }
    }
    std::cout << "i = " << i << "\n";
    std::cout << "j = " << j << "\n";
    if (l < j) {
        Qsort(A, l, j);
    }
    if (i < r) {
        Qsort(A, i, r);
    }

    for (int k = 0; k < (r - l + 1); ++k) {
        std::cout << A[k] << " ";
    }
    std::cout << "\n ";
}
int main() {
    int a;
    std::cout << "Please, enter the size of your array : ";
    std::cin >> a;

    int *A = new int[a];

    std::cout << "array : \n";
    for (int i = 0; i < a; ++i) {
        A[i] = rand() % 10;
        std::cout << A[i] << " ";
    }
    std::cout << "\n";
    Qsort(A, 0, a - 1);
    return 0;
}
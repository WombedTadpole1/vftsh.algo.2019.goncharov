#include <iostream>
#include <algorithm>

void Qsort (int* array, int left, int right) {
    int l = left, r = right;
    int middle = array[(l + r) / 2];
    while (l < r) {
        while (array[l] < middle) {
            ++l;
        }
        while (array[r] > middle) {
            --r;
        }
        if (l <= r) {
            int h = array[l];
            array[l] = array[r];
            array[r] = h;
            ++l;
            --r;
        }
    }
    if (left < r) {
        Qsort(array, left, r);
    }
    if (l < right) {
        Qsort(array, l, right);
    }
}


int main() {
    int n = 0;
    std::cout <<"Enter the number of items: \n";
    std::cin >> n;
    int *A = new int[n];

    std::cout <<"Your array: \n";

    for (int i = 0; i < n; i++) {
        A[i] = rand() % 10;
        std::cout << A[i] << " ";
    }
    int left = 0;
    int right = n - 1;

    Qsort(A, left, right);

    std::cout << "\n" << "Sorted array : \n";

    for (int i = 0; i < n; i++) {
        std::cout << A[i] << " ";
    }
    delete[] A;
    return 0;

}

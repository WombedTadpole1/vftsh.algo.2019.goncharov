#include <iostream>
#include <algorithm>
void Merge(int *A, int left, int middle, int right) {
    int k;
    int n1 = middle - left + 1;
    int n2 = right - middle;
    int *L = new int[n1];
    int *R = new int[n2];
    for (int i = 0; i < n1; i++) {
        L[i] = A[left + i];
    }
    for (int j = 0; j < n2; j++) {
        R[j] = A[middle + j + 1];
    }
    int i = 0;
    int j = 0;
    for (k = left; i < n1 && j < n2; k++) {
        if (L[i] < R[j]) {
            A[k] = L[i++];
        }
        else {
            A[k] = R[j++];
        }
    }
    while (i < n1) {
        A[k++] = L[i++];
    }
    while (j < n2) {
        A[k++] = R[j++];
    }
}
void MergeSort(int* A, int left, int right) {
    int middle;
    if (left < right) {
        middle = (left + right) / 2;
        MergeSort(A, left, middle);
        MergeSort(A,middle+1, right);
        Merge(A, left , middle, right);
    }
}
int main() {
    int n;
    std::cout << "Enter the number of items:" << "\n";
    std::cin >> n;
    int* A = new int[n];
    std::cout << "Your array1:" << "\n";
    for (int i = 0; i < n; i++) {
        A[i] = rand() % 10;
        std::cout << A[i] << " ";
    }
    std::cout << "\n";
    MergeSort(A, 0, n - 1);
    std::cout << "Sorted array:" << "\n";
    for (int i = 0; i < n; i++) {
    std::cout << A[i] << " ";
    }
    delete[] A;
    return 0;
}

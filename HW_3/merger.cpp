#include <iostream>
#include <cstdlib>
using namespace std;

void SuperMerger(int *D, int l, int m, int r) {
    int a = m - l + 1;
    int b = r - m;
    int *A = new int[a];
    int *B = new int[b];
    for (int i = 0; i < a; ++i) {
        A[i] = D[l + i];
    }
    for (int j = 0; j < b; ++j) {
        B[j] = D[m + j + 1];
    }
    int i = 0;
    int j = 0;
    int k = l;
    while ((i < a) && (j < b)) {
        if (A[i] < B[j]) {
            D[k] = A[i];
            ++i;
        } else {
            D[k] = B[j];
            ++j;
        }
        ++k;
    }
    while (i < a) {
        D[k++] = A[i++];
    }
    while (j < b) {
        D[k++] = B[j++];
    }
}

void SuperDivide(int *D, int l, int r)
{
    if (l < r)
    {
        int m = (l + r)/2;
        SuperDivide(D, l, m);
        SuperDivide(D, m + 1, r);
        SuperMerger(D, l, m , r);
    }

}

int main() {
    int a;
    cout << "Please, enter the size of your array: ";
    cin >> a;
    int *A = new int[a];
    cout << "Your array: ";
    for (int i = 0; i < a; ++i) {
        A[i] = rand() % 10;
        cout << A[i] << " ";
    }
    cout << "\n";
    SuperDivide(A, 0, a - 1);
    std::cout << "Sorted array: ";
    for (int i = 0; i < a; ++i){
        std::cout << A[i] << " ";
    }
    return 0;
}

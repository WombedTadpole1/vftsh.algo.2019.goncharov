#include <iostream>

using std::cout;
using std::cin;

void SuperMerger(int *d, int l, int m, int r) {
    int a = m - l + 1;
    int b = r - m;
    int *A = new int[a];
    int *B = new int[b];
    for (int i = 0; i < a; ++i) {
        A[i] = d[l + i];
    }
    for (int j = 0; j < b; ++j) {
        B[j] = d[m + j + 1];
    }
    int i = 0;
    int j = 0;
    int k = l;
    while ((i < a) && (j < b)) {
        if (A[i] < B[j]) {
            d[k] = A[i];
            ++i;
        }
        else {
            d[k] = B[j];
            ++j;
        }
        ++k;
    }
    while (i < a) {
        d[k++] = A[i++];
    }
    while (j < b) {
        d[k++] = B[j++];
    }
}

void SuperDivide(int *d, int l, int r)
{
    if (l < r)
    {
        int m = (l + r)/2;
        SuperDivide(d, l, m);
        SuperDivide(d, m + 1, r);
        SuperMerger(d, l, m , r);
    }

}

int main() {
    int a;
    cout << "Please, enter the size of your array: ";
    cin >> a;
    int *A = new int[a];
    cout << "Your array: ";
    for (int i = 0; i < a; ++i) {
        A[i] = rand() % 10;
        cout << A[i] << " ";
    }
    cout << "\n";
    SuperDivide(A, 0, a - 1);
    cout << "Sorted array: ";
    for (int i = 0; i < a; ++i){
        cout << A[i] << " ";
    }
    return 0;
}

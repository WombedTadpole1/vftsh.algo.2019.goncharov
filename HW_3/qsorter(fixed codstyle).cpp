#include <iostream>

void Qsort(int* a, int l, int r) {
    int s = (r + l ) / 2;
    int support = a[s];
    int i = l;
    int j = r;
    while (i <= j) {
        while (a[i] < support) {
            ++i;
        }
        while (a[j] > support) {
            --j;
        }
        if (i <= j) {
            int help = a[i];
            a[i] = a[j];
            a[j] = help;
            ++i;
            --j;
        }
    }
    if (l < j) {
        Qsort(a, l, j);
    }
    if (i < r) {
        Qsort(a, i, r);
    }
}

int main() {
    int a;
    std::cout << "Please, enter the size of your array: ";
    std::cin >> a;
    int *array = new int[a];
    std::cout << "Your array: ";
    
    for (int i = 0; i < a; ++i) {
        array[i] = rand() % 10;
        std::cout << array[i] << " ";
    }

    std::cout << "\n";
    Qsort(array, 0, a - 1);
    std::cout << "Sorted array: ";

    for (int i = 0; i < a; ++i) {
        std::cout << array[i] << " ";
    }
    return 0;
}
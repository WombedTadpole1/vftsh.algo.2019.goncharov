#include <iostream>

using std::cin;
using std::cout;

void Qsort(int* a, int l, int r) {
    int s = (r + l ) / 2;
    int support = a[s];
    int i = l;
    int j = r;
    while (i <= j) {
        while (a[i] < support) {
            ++i;
        }
        while (a[j] > support) {
            --j;
        }
        if (i <= j) {
            int help = a[i];
            a[i] = a[j];
            a[j] = help;
            ++i;
            --j;
        }
    }
    if (l < j) {
        Qsort(a, l, j);
    }
    if (i < r) {
        Qsort(a, i, r);
    }
}

int main() {
    int n;
    int a = 0;
/*    Счетчики, которые нужны , чтобы проверить массив
    на все четные или нечетные*/
    int b = 0;
    cin >> n;
    int* Array = new int[n];
    for (int i = 0; i < n; ++i) {
        cin >> Array[i];
    }
    for (int i = 0; i < n; ++i) {
        if (Array[i] % 2 == 0) {
            ++a;
        }
        else {
            ++b;
        }
    }
    if ( a == n || b == n) {
        for (int i = 0; i < n; ++i) {
            cout << Array[i] << " ";
        }
        return 0;
    }
    Qsort(Array, 0, n - 1);
    for (int i = 0; i < n; ++i) {
        cout << Array[i] << " ";
    }
    return 0;
}
#include <iostream>
#include <deque>

using std::cin;
using std::cout;
using std::deque;

int main() {
    int n,a;
    std::cin >> n;
    deque<int> numbers;

    for (int i = 0; i < n; i++) {
        cin >> a;
        numbers.push_back(a);
    }

    while (numbers.size() >= 2) {

        if ((numbers.at(0) + numbers.at(1)) % 2 == 0 ) {
            numbers.push_back(numbers.at(0) + numbers.at(1));
        }

        else {
            numbers.push_back(numbers.at(0) - numbers.at(1));
        }
        numbers.pop_front();
        numbers.pop_front();
    }
    cout << numbers.at(0);
    return 0;
}

#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::vector;

int main() {
    int n, help;
    cin >> n;
    if (n == 1 || n == 2) {
        cout << -1;
        return  0;
    }
    vector<int> a(n);
    a[0] = n;
    help = n;
    for (int i = 1; i < n; ++i) {
        a[i] = --help;
    }
    for (int k = 0; k < n; ++k) {
        cout << a[k] << " ";
    }
    return 0;
}
#include <iostream>
#include <deque>
using std::cin;
using std::cout;
using std::deque;
int Sum (deque<int> A, int length) {
    int S1 = 0;
    int S2 = 0;
    for (int i = 0; i < length / 2; ++i) {
        S1 = S1 + A[i];
    }
    for (int i = length / 2; i < length; ++i) {
        S2 = S2 + A[i];
    }
    if (S1 == S2) {
        return 1;
    }
    else {
        return 0;
    }
}

int main() {
    int n, x;
    bool check;
    cin >> n;
    deque<int> A;
    for (int i = 0; i < 2 * n; ++i) {
        cin >> x;
        A.push_back(x);
    }
    for (int i = 0; i < 2 * n - 1; ++i) {
        if (A[i + 1] == A[i]) {
            check = 1;
        }
        else {
            check = 0;
            break;
        }
    }
    if (check == 1) {
        cout << -1;
        return 0;
    }
    else {
        int i = 0;
        int j = 2 * n - 1;
        while ((i < j) && (Sum(A, 2 * n) == 1)) {
            if (A[i] != A[j]) {
                int help = A[i];
                A[i] = A[j];
                A[j] = help;
            }
            ++i;
            --j;
        }
    }
    if (Sum(A, 2 * n) == 1) {
        cout << -1;
    }
    else {
        for (int k = 0; k < 2 * n; ++k) {
            cout << A[k] << " ";
        }
    }
    return 0;
}
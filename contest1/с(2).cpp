#include <iostream>
#include <vector>

using std::vector;
using std::cin;
using std::cout;

void Qsort(vector<int> &a, int l, int r) {
    int s = (r + l ) / 2;
    int support = a[s];
    int i = l;
    int j = r;
    while (i <= j) {
        while (a[i] < support) {
            ++i;
        }
        while (a[j] > support) {
            --j;
        }
        if (i <= j) {
            int help = a[i];
            a[i] = a[j];
            a[j] = help;
            ++i;
            --j;
        }
    }
    if (l < j) {
        Qsort(a, l, j);
    }
    if (i < r) {
        Qsort(a, i, r);
    }
}

int main() {
    int n;
    int help;
    int isEven = 0;
    int isOdd = 0;
    cin >> n;
    vector<int> array;
    for (int i = 0; i < n; ++i) {
        cin >> help;
        array.push_back(help);
    }
    for (int i = 0; i < array.size(); ++i) {
        if (array.at(i) % 2 == 0) {
            ++isEven;
        }
        else {
            ++isOdd;
        }
    }

    if ( isEven == array.size() || isOdd == array.size()) {
        for (int i = 0; i < array.size(); ++i) {
            cout << array[i] << " ";
        }
        return 0;
    }
    Qsort(array, 0, array.size() - 1);
    for (int i = 0; i < array.size(); ++i) {
        cout << array[i] << " ";
    }
    return 0;
}
